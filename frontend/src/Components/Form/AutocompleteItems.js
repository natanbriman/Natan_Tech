import { isEmpty } from 'lodash';
import { Typeahead } from 'react-bootstrap-typeahead';
import 'react-bootstrap-typeahead/css/Typeahead.css';
import Form from 'react-bootstrap/Form';

const AutocompleteItems = ({ items, label, onChange, inputProps, labelKey, key }) => {
  return (
    !isEmpty(items) && (
      <Form.Group className='mb-3' key={key}>
        <Form.Label>
          <b>{label}</b>
        </Form.Label>

        <Typeahead
          placeholder={items[0].name}
          id='Autocomplete'
          options={items}
          onChange={onChange}
          highlightOnlyResult
          paginate
          maxResults={7}
          {...inputProps}
          labelKey={labelKey}
        />
      </Form.Group>
    )
  );
};

export default AutocompleteItems;
